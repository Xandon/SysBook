/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.es.Telas;

/**
 *
 * @author sala302b
 */
public class JFrameMenuPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form JFrameMenuPrincipal
     */
    public JFrameMenuPrincipal() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jRadioButtonMenuItemAUTORNOMENU = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItemGENERONOMENU = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItemGENERONOLIVRO = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItemCLIENTENOMENU = new javax.swing.JRadioButtonMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jRadioButtonMenuItemPESQUISARCLIENTENOMENU = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItemPESQUISARLIVRONOMENU = new javax.swing.JRadioButtonMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/senac/es/Telas/imagens/real-gabinete-portugues-rio-de-janeiro-Edu-Mendes-biblioteca-2.jpg"))); // NOI18N

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/senac/es/Telas/imagens/real-gabinete-portugues-rio-de-janeiro-Edu-Mendes-biblioteca-2.jpg"))); // NOI18N

        jLabel2.setText("Versão 5.2");

        jMenu1.setText("Cadastro");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jRadioButtonMenuItemAUTORNOMENU.setSelected(true);
        jRadioButtonMenuItemAUTORNOMENU.setText("Autor");
        jRadioButtonMenuItemAUTORNOMENU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItemAUTORNOMENUActionPerformed(evt);
            }
        });
        jMenu1.add(jRadioButtonMenuItemAUTORNOMENU);

        jRadioButtonMenuItemGENERONOMENU.setSelected(true);
        jRadioButtonMenuItemGENERONOMENU.setText("Gênero");
        jRadioButtonMenuItemGENERONOMENU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItemGENERONOMENUActionPerformed(evt);
            }
        });
        jMenu1.add(jRadioButtonMenuItemGENERONOMENU);

        jRadioButtonMenuItemGENERONOLIVRO.setSelected(true);
        jRadioButtonMenuItemGENERONOLIVRO.setText("Livro");
        jRadioButtonMenuItemGENERONOLIVRO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItemGENERONOLIVROActionPerformed(evt);
            }
        });
        jMenu1.add(jRadioButtonMenuItemGENERONOLIVRO);

        jRadioButtonMenuItemCLIENTENOMENU.setSelected(true);
        jRadioButtonMenuItemCLIENTENOMENU.setText("Cliente");
        jRadioButtonMenuItemCLIENTENOMENU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItemCLIENTENOMENUActionPerformed(evt);
            }
        });
        jMenu1.add(jRadioButtonMenuItemCLIENTENOMENU);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Pesquisar");

        jRadioButtonMenuItemPESQUISARCLIENTENOMENU.setSelected(true);
        jRadioButtonMenuItemPESQUISARCLIENTENOMENU.setText("Cliente");
        jRadioButtonMenuItemPESQUISARCLIENTENOMENU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItemPESQUISARCLIENTENOMENUActionPerformed(evt);
            }
        });
        jMenu2.add(jRadioButtonMenuItemPESQUISARCLIENTENOMENU);

        jRadioButtonMenuItemPESQUISARLIVRONOMENU.setSelected(true);
        jRadioButtonMenuItemPESQUISARLIVRONOMENU.setText("Livro");
        jRadioButtonMenuItemPESQUISARLIVRONOMENU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItemPESQUISARLIVRONOMENUActionPerformed(evt);
            }
        });
        jMenu2.add(jRadioButtonMenuItemPESQUISARLIVRONOMENU);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Empréstimo");

        jMenuItem1.setText("Data do Empréstimo");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem1);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioButtonMenuItemPESQUISARCLIENTENOMENUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItemPESQUISARCLIENTENOMENUActionPerformed
    new JFramePesquisaCliente().setVisible(true);
    }//GEN-LAST:event_jRadioButtonMenuItemPESQUISARCLIENTENOMENUActionPerformed

    private void jRadioButtonMenuItemGENERONOMENUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItemGENERONOMENUActionPerformed
    new JFrameGenero().setVisible(true); 
    }//GEN-LAST:event_jRadioButtonMenuItemGENERONOMENUActionPerformed

    private void jRadioButtonMenuItemPESQUISARLIVRONOMENUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItemPESQUISARLIVRONOMENUActionPerformed
     new JFramePesquisaLivro().setVisible(true); 
    }//GEN-LAST:event_jRadioButtonMenuItemPESQUISARLIVRONOMENUActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
     
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void jRadioButtonMenuItemAUTORNOMENUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItemAUTORNOMENUActionPerformed
    
     new JFrameAutor().setVisible(true);   
     
    }//GEN-LAST:event_jRadioButtonMenuItemAUTORNOMENUActionPerformed

    private void jRadioButtonMenuItemGENERONOLIVROActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItemGENERONOLIVROActionPerformed
        new JFrameLivro().setVisible(true); 
    }//GEN-LAST:event_jRadioButtonMenuItemGENERONOLIVROActionPerformed

    private void jRadioButtonMenuItemCLIENTENOMENUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItemCLIENTENOMENUActionPerformed
        new JFrameCliente().setVisible(true); 
    }//GEN-LAST:event_jRadioButtonMenuItemCLIENTENOMENUActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
    new JFrameEmprestimo().setVisible(true); 
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameMenuPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItemAUTORNOMENU;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItemCLIENTENOMENU;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItemGENERONOLIVRO;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItemGENERONOMENU;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItemPESQUISARCLIENTENOMENU;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItemPESQUISARLIVRONOMENU;
    // End of variables declaration//GEN-END:variables
}
